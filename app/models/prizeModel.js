// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// 
const Schema = mongoose.Schema;

const prizeSchema = new Schema ({
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("Prize", prizeSchema);