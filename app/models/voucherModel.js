// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// 
const Schema = mongoose.Schema;

//
const voucherSchema = new Schema ({
    code: {
        type: String,
        unique: true,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
    note: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model("Voucher", voucherSchema);