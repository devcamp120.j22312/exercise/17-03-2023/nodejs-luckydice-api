const mongoose = require('mongoose');

const prizeHistoryModel = require('../models/prizeHistoryModel');

const createPrizeHistory = (request, response) => {
    const body = request.body;

    const newPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        prize: mongoose.Types.ObjectId(),
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    prizeHistoryModel.create(newPrizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Create new prize history successfully',
                data: data
            })
        }
    })
}

const getAllPrizeHistory = (request, response) => {
    let userId = request.query.userId;
    let condition = {};
    if (userId) {
        condition.userId = userId;
    }

    prizeHistoryModel.find(condition)
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Error 500: Internal server error',
                    message: error.message
                })
            }
            else {
                return response.status(200).json({
                    status: 'Get all prize history successfully',
                    data: data
                })
            }
        })
}

const getPrizeHistoryById = (request, response) => {
    const prizeHistoryId = request.params.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Prize History ID is not valid'
        })
    }

    prizeHistoryModel.findById(prizeHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Get prize history id ${prizeHistoryId} successfully `,
                data: data
            })
        }
    })
}

const updatePrizeHistoryById = (request, response) => {
    const prizeHistoryId = request.params.prizeHistoryId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Prize History ID is not valid'
        })
    }

    const updatePrizeHistory = {
        user: body.user,
        prize: body.prize
    }

    prizeHistoryModel.findByIdAndUpdate(prizeHistoryId, updatePrizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Update prize history id ${prizeHistoryId} successfully `,
                data: data
            })
        }
    })
}

const deletePrizeHistoryById = (request, response) => {
    const prizeHistoryId = request.params.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Prize History ID is not valid'
        })
    }

    prizeHistoryModel.findByIdAndDelete(prizeHistoryId, (error) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Delete prize history id ${prizeHistoryId} successfully `
            })
        }
    })
}
const getPrizeHistoryByUsername = (request, response) => {
    const username = request.query.username;

    if (username) {
        prizeHistoryModel.findOne({ username }, (errorFindUser, userExist) => {
            if (errorFindUser) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                if (!userExist) {
                    return response.status(500).json({
                        status: 'Username is not valid',
                        data: []
                    })
                } else {
                    prizeHistoryModel.findOne({ username }, (error, data) => {
                        if (error) {
                            return response.status(500).json({
                                status: 'Internal server error',
                                message: error.message
                            })
                        } else {
                            return response.status(200).json({
                                status: `Get Prize History Of User ${username}`,
                                prizehistory: data.prizehistory
                            })
                        }
                    })
                }
            }
        })
    }
}

module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById,
    getPrizeHistoryByUsername
}