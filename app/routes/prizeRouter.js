// Khai báo thư viện
const express = require('express');

// Import user Controller
const prizeController = require('../controllers/prizeController');

// Khai báo router
const prizeRouter = express.Router();

// Create User
prizeRouter.post('/prizes', prizeController.createPrize);

// Get All User
prizeRouter.get('/prizes', prizeController.getAllPrize);

// Get User By Id
prizeRouter.get('/prizes/:prizeId', prizeController.getPrizeById);

// Update User By Id
prizeRouter.put('/prizes/:prizeId', prizeController.updatePrizeById);

// Delete User By Id
prizeRouter.delete('/prizes/:prizeId', prizeController.deletePrize);

module.exports = prizeRouter