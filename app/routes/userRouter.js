// Khai báo thư viện
const express = require('express');

// Import user Controller
const userController = require('../controllers/userController');

// Khai báo router
const userRouter = express.Router();

// Create User
userRouter.post('/users', userController.createUser);

// Get All User
userRouter.get('/users', userController.getAllUser);

// Get User By Id
userRouter.get('/users/:userId', userController.getUserById);

// Update User By Id
userRouter.put('/users/:userId', userController.updateUserById);

// Delete User By Id
userRouter.delete('/users/:userId', userController.deleteUser);

module.exports = userRouter