// Import expressJS
const express = require('express');
// Import thư viện path
const path = require('path');
// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Khai báo port
const port = 8000;

// Khởi tạo app
const app = express();

// Import Model 
const userModel = require('./app/models/userModel');
const diceHistoryModel = require('./app/models/diceHistoryModel');
const prizeModel = require('./app/models/prizeModel');
const voucherModel = require('./app/models/voucherModel');
const prizeHistoryModel = require('./app/models/prizeHistoryModel');
const voucherHistoryModel = require('./app/models/voucherHistoryModel');

// Khai báo router 
const randomNumberRouter = require('./app/routes/randomNumberRouter');
const userRouter = require('./app/routes/userRouter');
const diceHistoryRouter = require('./app/routes/diceHistoryRouter');
const prizeRouter = require('./app/routes/prizeRouter');
const voucherRouter = require('./app/routes/voucherRouter');
const prizeHistoryRouter = require('./app/routes/prizeHistoryRouter');
const voucherHistoryRouter = require('./app/routes/voucherHistoryRouter');

// App sử dụng middleware
app.use('/', randomNumberRouter);
app.use('/', userRouter);
app.use('/', diceHistoryRouter);
app.use('/', prizeRouter);
app.use('/', voucherRouter);
app.use('/', prizeHistoryRouter);
app.use('/', voucherHistoryRouter);

// Cấu hình request đọc được body json
app.use(express.json());

// Hiển thị hình ảnh
app.use(express.static(__dirname + "/views"));

// Call api chạy project 
app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname + "/views/Task 31.30.html"));
})

// Connect MongoDB
main().catch(err => console.log(err));
async function main() {
    await mongoose.connect('mongodb://127.0.0.1:27017/CRUD_LuckyDice');
    console.log("Connect MongoDB Successfully");
}

// Khởi động app
app.listen = (port, () => {
    console.log("App listening on port: ", port)
})
